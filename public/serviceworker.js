const CACHE_NAME="version-1";
const urlsToCache = ["index.html", "offline.html"];

const self=this;

// install 
self.addEventListener('install',(event)=>{
    event.waitUntil(
        caches.open(CACHE_NAME)
        .then((cache)=>{
            console.log('Opened cache');

            return cache.addAll(urlsToCache);
        })
    )
});

// listen for requests
self.addEventListener("fetch", (event) => {
    event.respondWith(
        caches.match(event.requst)
        .then(()=>{
            return fetch(event.requst)
            .catch(()=>caches.match('offline.html'))
        })
    )
});

// Activate SW
self.addEventListener("activate", (event) => {
    const cacheWithList=[];
    cacheWithList.push(CACHE_NAME);
    event.waitUntil(
        caches.keys().then((cacheNames)=>Promise.all(
            cacheNames.map((cacheName)=>{
                if(!cacheWithList.includes(cacheName)){
                    return caches.delete(cacheName);
                }
            })
        ))
    )
});