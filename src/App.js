import React,{useState} from 'react';
import { FetchWeather } from './api/FetchWeather';

function App() {
  const [query,setQuery]=useState('');
  const [weather,setWeather]=useState({});
  const [icon,setIcon]=useState('')

  const handleChange=(event)=>{
    setQuery(event.target.value)
  }

  const serach=async(e)=>{
    if(e.key === 'Enter'){
      const data=await FetchWeather(query)
console.log(data)
      setWeather(data);
      setQuery('');
      console.log(data.weather[0].icon);
      setIcon(data.weather[0].icon);
    }
  }

  // var iconSrc="http://openweathermap.org/img/w"+weather['weather'][0].icon+".png"

  return (
    <div className="main-container">
      <input
        type="text"
        className="search"
        placeholder="Search city name..."
        value={query}
        onChange={handleChange}
        onKeyPress={serach}
      />

      {weather.main && (
        <div className="info-box">
          <div className="city">
            <div className="first-line-info">
              <div className="info">
                <img
                  className="city-icon"
                  src={`https://openweathermap.org/img/wn/${icon}@2x.png`}
                />
              </div>

              <h2 className="city-name">
                <span>{weather.name}</span>
                <sup>{weather.sys.country}</sup>
              </h2>
            </div>

            <div className="city-max-min-temp">
              <h5 style={{ marginRight: "20px" }}>
                H: {Math.round(weather.main.temp_max)}{" "}
                <sup style={{ marginRight: "20px" }}>&deg; C</sup>
                L: {Math.round(weather.main.temp_min)} <sup>&deg; C</sup>
              </h5>
            </div>

            <div className="info">
              <div> Condition: {weather.weather[0].description}</div>
            </div>
            <div className="city-temp">
              Temp: {Math.round(weather.main.temp)}
              <sup>&deg; C</sup>
            </div>
            <div className="city-temp">
              Humidity: {Math.round(weather.main.humidity)}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default App